//
// Created by Yaokai Liu on 11/29/22.
//

#ifndef X_XDEF_H
#define X_XDEF_H

#include "xtypes.h"

#define sizeof  sizeof
#ifndef offsetof
    #define offsetof(_TYPE, _MEMBER) ((xSize) &(nullptrof(_TYPE)->_MEMBER))
#endif
#ifndef nullptrof
    #define nullptrof(_TYPE)    ((_TYPE *) 0)
#endif

#ifndef bitsof
    #define bitsof(_TYPE)    (sizeof(_TYPE) << 3)
#endif
#ifndef lenof
    #define lenof(_ARRAY_LITERAL)    (sizeof(_ARRAY_LITERAL) / sizeof(_ARRAY_LITERAL[0]))
#endif

#define typeof typeof


#ifdef XDEF_USING_C_STD_DEF
    #ifndef NULL
        #define NULL    nullptrof(xVoid)
    #endif

    #if !defined(true) || !defined(false)
        #define true	((xBool)+1u)
        #define false	((xBool)+0u)
    #endif
#endif


#endif //X_XDEF_H
